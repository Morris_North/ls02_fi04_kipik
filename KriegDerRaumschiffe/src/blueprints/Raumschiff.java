package blueprints;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Raumschiff Klasse.
 *
 * @author  Erkan Kipik
 * @version 0.1
 */
public class Raumschiff {
	
	/** Der Schiffsname */
	private String schiffsname;
	
	/** Level der Energieversorgung. */
	private int energieversorgung;
	
	/** Level der schutzschilde. */
	private int schutzschilde;
	
	/** Zustand der Lebenserhaltungssysteme. */
	private int lebenserhaltungsSysteme;
	
	/** Integrit�t der Huelle. */
	private int huelle;
	
	/** Anzahl der Photonentorpedos. */
	private int photonentorpedos;
	
	/** Anzahl der  Reparatur Androiden. */
	private int reparaturAndroiden;
	
	/** Der Broadcastkommnikator. */
	private ArrayList<String> broadcastKommnikator;
	
	/** Das Ladungsverzeichnis. */
	private ArrayList<Ladung> ladungsverzeichnis;
	
	/**
	 * Standard Konstruktor, initiert die Arrays, l�sst den rest auf null.
	 */
	public Raumschiff() {
		this.setBroadcastKommunikator(new ArrayList<String>());
		this.setLadungsverzeichnis(new ArrayList<Ladung>());
	}
	
	/**
	 * Voller Konstruktor, setzt alle Attribute auf die Parameter, f�hr den Default Konstruktor aus.
	 *
	 * @param schiffsname 				Name des Schiffs.
	 * @param energieversorgung 		Energie level in prozent.
	 * @param schutzschilde 			Schild integrit�t in prozent.
	 * @param lebenserhaltungsSysteme   Zustand der Lebenserhaltungssysteme
	 * @param huelle 					Huellen integrit�t in prozent.
	 * @param photonentorpedos 			Anzahl der geladenen Photonentorpedos.
	 * @param reparaturAndroiden 		Anzahl der Repartur Androiden.
	 */
	public Raumschiff(String schiffsname, int energieversorgung, int schutzschilde, int lebenserhaltungsSysteme, int huelle, int photonentorpedos, int reparaturAndroiden) {
		this();
		this.setSchiffsname(schiffsname);
		this.setEnergieversorgung(energieversorgung);
		this.setSchutzschilde(schutzschilde);
		this.setLebenserhaltungsSysteme(lebenserhaltungsSysteme);
		this.setHuelle(huelle);
		this.setPhotonentorpedos(photonentorpedos);
		this.setReparaturAndroiden(reparaturAndroiden);
	}
	
	
	/**
	 * Setzt den Schiffsnamen.
	 *
	 * @param schiffsname Der neue Schiffsname.
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	/**
	 * Setzt das Level der Energieversorgung.
	 *
	 * @param energieversorgung Das neue Level der Energieversorgung.
	 */
	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	
	/**
	 * Setzt das Level der Schutzschilde.
	 *
	 * @param schutzschilde Das neue level der Schutzschilde.
	 */
	public void setSchutzschilde(int schutzschilde) {
		this.schutzschilde = schutzschilde;
	}
	
	/**
	 * Setzt den Zustand der Lebeneserhaltungssysteme.
	 *
	 * @param lebenserhaltungsSysteme Der neue Zustand der Lebenserhaltungssysteme.
	 */
	public void setLebenserhaltungsSysteme(int lebenserhaltungsSysteme) {
		this.lebenserhaltungsSysteme = lebenserhaltungsSysteme;
	}
	
	/**
	 * Setzt die Integrit�t der H�lle.
	 *
	 * @param huelle Die neue Integrit�t der H�lle.
	 */
	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}
	
	/**
	 * Setzt die Anzahl der Photonentorpedos.
	 *
	 * @param photonentorpedos Die neue ANzahl der Photonentorpedos.
	 */
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	
	/**
	 * Setzt die Anzahl der Reparatur Androiden.
	 *
	 * @param reparaturAndroiden Die neue Anzahl der Reparatur Androiden.
	 */
	public void setReparaturAndroiden(int reparaturAndroiden) {
		this.reparaturAndroiden = reparaturAndroiden;
	}
	
	/**
	 * Setzt den Broadcastkommunikator.
	 *
	 * @param broadcastKommnikator Der neue Broadcastkommunikator.
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommnikator) {
		this.broadcastKommnikator = broadcastKommnikator;
	}
	
	/**
	 * Setzt das Ladungsverzeichnis..
	 *
	 * @param ladungsverzeichnis Das neue Ladungsverzeichnis.
	 */
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	/**
	 * Gibt den Schiffsnamen.
	 *
	 * @return der Schiffsname.
	 */
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	/**
	 * Gibt das Energieversorungs level.
	 *
	 * @return das Energieversorungs level.
	 */
	public int getEnergieversorgung() {
		return this.energieversorgung;
	}
	
	/**
	 * Gibt das Schutzschilde level.
	 *
	 * @return das Schutzschilde level.
	 */
	public int getSchutzschilde() {
		return this.schutzschilde;
	}
	
	/**
	 * Gibt den Zustand der Lebenserhaltungssysteme.
	 *
	 * @return der Zustand der Lebenserhaltungssysteme.s
	 */
	public int getLebenserhaltungsSysteme() {
		return this.lebenserhaltungsSysteme;
	}
	
	/**
	 * Gibt die Integrit�t der H�lle.
	 *
	 * @return DIe Integrit�t der H�lle,
	 */
	public int getHuelle() {
		return this.huelle;
	}
	
	/**
	 * Gibt die Anzahl der Photonentorpedos.
	 *
	 * @return Die Anzahl der Photonentorpedos.
	 */
	public int getPhotonentorpedos() {
		return this.photonentorpedos;
	}
	
	/**
	 * Gibt die Anzahl der Reparatur Androiden.
	 *
	 * @return Die Anzahl der Reparatur Androiden.
	 */
	public int getReparaturAndroiden() {
		return this.reparaturAndroiden;
	}
	
	/**
	 * Gibt den Broadcaskommunikator.
	 *
	 * @return Der Brodcastkommunikator
	 */
	public ArrayList<String> getBroadcastKommunikator() {
		return this.broadcastKommnikator;
	}
	
	/**
	 * Gibt das Ladungsverzeichnis.
	 *
	 * @return Das Ladungsverzeichnis.
	 */
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return this.ladungsverzeichnis;
	}
	
	/**
	 * F�gt dem Ladungsverzeichnis eine neue Ladung hinzu.
	 *
	 * @param neueLadung die hinzuzuf�gende Ladung.
	 */
	//Methods
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * Gibt den gesamten Zustand des Schiffes aus.
	 */
	public void zustandAusgeben() {
		System.out.println("Schiffszustand: ");
		System.out.println("Schiffsname: "+this.getSchiffsname());
		System.out.println("Energieversorgung: "+this.getEnergieversorgung()+" %");
		System.out.println("Schutzschilde: "+this.getSchutzschilde()+" %");
		System.out.println("Lebenserhaltungs Systeme: "+this.getLebenserhaltungsSysteme()+" %");
		System.out.println("Huelle: "+this.getHuelle()+" %");
		System.out.println("Photonentorpedos: "+this.getPhotonentorpedos());
		System.out.println("Reparaturandroiden: "+this.getReparaturAndroiden());
	}

	/**
	 * Gibt das gesamte Ladungsverzeichnis aus.
	 */
	public void ladgunsverzeichnisAusgeben() {
		System.out.println("Ladungen: ");
		for(Ladung l : this.ladungsverzeichnis) {
			System.out.println("Ladungs Bezeichnung: " +l.getBezeichnung());
			System.out.println("Ladungs Anzahl: " +l.getAnzahl());
		}
	}
	
	/**
	 * Schie�t einen einzelnen Photonentorpedo ab auf das gegebene Schiff und verringert 
	 * die Anahl der Torpedos um 1.
	 *
	 * @param raumschiff Das Ziel Raumschiff.
	 */
	public void photonentorpedoAbschie�en(Raumschiff raumschiff) {
		if(this.getPhotonentorpedos() == 0) {
			this.nachrichtenAnAlle("-=*Click*=-");
		}else {
			this.setPhotonentorpedos(this.getPhotonentorpedos()-1);
			this.nachrichtenAnAlle("Photonentorpedo abgeschossen.");
			raumschiff.trefferVermerken();
		}
	}
	
	/**
	 * Schie�t die Phaserkanonen ab auf das gegebene Schiff und verringert die Energieversorgung um 50%.
	 * Pr�ft vorher ab ob das Energie level ausreichend ist. F�r beide F�lle wird eine Nachricht
	 * an alle geschickt.
	 *
	 * @param raumschiff Das Ziel Raumschiff.
	 */
	public void phaserkanonenAbschie�en(Raumschiff raumschiff) {
		if(this.getEnergieversorgung() < 50) {
			this.nachrichtenAnAlle("-=*Click*=-");
		}else {
			this.setEnergieversorgung(this.getEnergieversorgung()-50);
			this.nachrichtenAnAlle("Phaserkanone abgeschossen.");
			raumschiff.trefferVermerken();
		}
	}
	
	/**
	 * Treffer vermerken. Sendet eine Nachricht an alle welches Schiff getroffen wurde.
	 * Berechnet die werte des getroffenene Schiffes neu. Gibt auch an ob ein Schiff 
	 * vollst�ndig zerst�rt wurde.
	 */
	public void trefferVermerken() {
		this.nachrichtenAnAlle(this.getSchiffsname()+" wurde getroffen.");
		
		this.setSchutzschilde(this.getSchutzschilde()-50);
		if(this.getSchutzschilde() < 50) {
			this.setHuelle(this.getHuelle()-50);
			this.setEnergieversorgung(this.getEnergieversorgung()-50);
			if(this.getHuelle() <= 0) {
				this.setLebenserhaltungsSysteme(0);
				this.nachrichtenAnAlle("Lebenserhaltungs Systeme von "+this.getSchiffsname()+" wurden vollst�ndig zerst�rt.");
			}
		}
	}

	/**
	 * Gibt die Logbuch eintr�ge aus.
	 */
	public void logbuchEintraegeAusgeben() {
		for(String msg : this.getBroadcastKommunikator()) {
			System.out.println(msg);
		}
	}
	
	/**
	 * Sendet eine Nachrichten an alle und schreibt diese ins Logbuch.
	 *
	 * @param nachricht Die Nachricht die gesendet werden soll.
	 */
	public void nachrichtenAnAlle(String nachricht) {
		this.broadcastKommnikator.add(nachricht);
		System.out.println(nachricht);
	}
	
	/**
	 *  L�dt die gegebene Anzahl von Photonentorpedos aus dem Ladungsverzeichnisladen.
	 *  �berpr�ft ob die gegebene Menge geladen werden kann, l�dt ansonsten die vorhandene 
	 *  Anzahl und verringert diese im Ladungsverzeichnis.
	 *
	 * @param anzahl Die Anzahl der zu ladenen Photonentorpedos.
	 */
	public void photonentorpedosLaden(int anzahl) {
		for(Ladung l : this.getLadungsverzeichnis()) {
			if(l.getBezeichnung().equalsIgnoreCase("Photonentorpedo")) {
				int x = Math.min(l.getAnzahl(), anzahl);
				this.setPhotonentorpedos(x);
				l.setAnzahl(l.getAnzahl()-x);
				return;			
			}
		}
		this.nachrichtenAnAlle("Keine Photonentorpedos vorhanden.");
		this.nachrichtenAnAlle("-=*Click*=-");
	}
	
	/**
	 * R�umt das Ladungsverzeichnis auf. Entfernt alle eintr�ge die auf 0 gefallen sind.
	 */
	public void ladungsverzeichnisAufraeumen() {
		Iterator<Ladung> ite = this.ladungsverzeichnis.iterator();
		while(ite.hasNext()) {
			Ladung l = ite.next();
			if(l.getAnzahl() <= 0) {
				ite.remove();
			}
		}
	}

	/**
	 * Setzt die gegebene Anzahl an Reparatur Androiden auf die gegebene Teile des Schiffes ein.
	 * Brechnet dazu einen Zufallswert und jagt diesen durch eine Formel.
	 * Alle true Teile des Schiffes werden um diesen Wert erh�ht.
	 *
	 * @param anzahl 					Anzahl der einzusetzenden Raparatur Androiden.
	 * @param energieversorgung 		Bool f�r diesen Teil des Schiffes.
	 * @param schutzschilde				Bool f�r diesen Teil des Schiffes.
	 * @param lebenserhaltungsSysteme 	Bool f�r diesen Teil des Schiffes.
	 * @param huelle the huelle			Bool f�r diesen Teil des Schiffes.
	 */
	public void reparaturAndroidenEinsetzen(int anzahl, boolean energieversorgung, boolean schutzschilde, boolean lebenserhaltungsSysteme, boolean huelle) {
		int rnd = (int) (Math.random()*100);
		int strukturen = 0;
		anzahl = Math.min(this.getReparaturAndroiden(), anzahl);
		this.setReparaturAndroiden(this.getReparaturAndroiden()-anzahl);
		
		if(energieversorgung) strukturen ++;
		if(schutzschilde) strukturen ++;
		if(lebenserhaltungsSysteme) strukturen ++;
		if(huelle) strukturen ++;
		
		int repairFor = rnd * anzahl / strukturen;
		
		if(energieversorgung) this.setEnergieversorgung(this.getEnergieversorgung()+repairFor);
		if(schutzschilde) this.setEnergieversorgung(this.getEnergieversorgung()+repairFor);
		if(lebenserhaltungsSysteme) this.setEnergieversorgung(this.getEnergieversorgung()+repairFor);
		if(huelle) this.setEnergieversorgung(this.getEnergieversorgung()+repairFor);
	}
}

