package blueprints;

/**
 * Ladung Klasse.
 *
 * @author  Erkan Kipik
 * @version 0.1
 */
public class Ladung {
	
	/** Die bezeichnung der Ladung. */
	private String bezeichnung;
	
	/** Die Anzahl der Ladung. */
	private int anzahl;
	
	/**
	 * Standard Konstuktor. Setzt die werte auf "Nichts" und 0.
	 */
	public Ladung() {
		this.bezeichnung = "Nichts";
		this.anzahl = 0;
	}
	
	/**
	 * Voller Konstruktor, setzt alle Attribute auf die Parameter.
	 *
	 * @param bezeichnung Die Bezeichnung der Ladung.
	 * @param anzahl Die Anzahl der Ladung.
	 */
	public Ladung(String bezeichnung, int anzahl) {
		setBezeichnung(bezeichnung);
		setAnzahl(anzahl);
	}
	
	/**
	 * Setzt die Bezeichnung der Ladung.
	 *
	 * @param bezeichnung Die neue Bezeichnung der Ladung.
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	/**
	 * Setzt die Anzahl der Ladung.
	 *
	 * @param anzahl Die neue Anzahl der Ladung.
	 */
	public void setAnzahl(int anzahl) {
		if(anzahl < 0) {
			this.anzahl = 0;
		}else {
			this.anzahl = anzahl;
		}
	}
	
	/**
	 * Gibt die Bezeichnung der Ladung.
	 *
	 * @return Die Bezeichnung der Ladung.
	 */
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	/**
	 * Gibt die Anzahl der Ladung.
	 *
	 * @return Die Anzahl der Ladung.
	 */
	public int getAnzahl() {
		return this.anzahl;
	}
}
