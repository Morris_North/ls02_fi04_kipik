package test;
import blueprints.*;

public class RaumschiffTest {
	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
		klingonen.addLadung(new Ladung("Ferrengin Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));

		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5);
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));
		
		klingonen.photonentorpedoAbschießen(romulaner);
		romulaner.phaserkanonenAbschießen(klingonen);
		vulkanier.nachrichtenAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandAusgeben();
		klingonen.ladgunsverzeichnisAusgeben();
		vulkanier.reparaturAndroidenEinsetzen(5, true, true, true, true);
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedoAbschießen(romulaner);
		klingonen.photonentorpedoAbschießen(romulaner);
		
		System.out.println("Klingonen---------------------");
		klingonen.zustandAusgeben();
		klingonen.ladgunsverzeichnisAusgeben();
		System.out.println("Romulaner---------------------");
		romulaner.zustandAusgeben();
		romulaner.ladgunsverzeichnisAusgeben();
		System.out.println("Vulkanier---------------------");
		vulkanier.zustandAusgeben();
		vulkanier.ladgunsverzeichnisAusgeben();
		
	}
}
