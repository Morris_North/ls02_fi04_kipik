/**
 * Created / Modified by 
 + @author Erkan Kipik
 * @date 2021-01-28
 * DO NOT COPY PASTE
 */
package angestelltenverwaltung;

public class Angestellter {
	
	//Attributes
	private String name;
	private double gehalt;
	
	//Constructor - Default is default
	public Angestellter() {
		//Nicht n�tig, belegt speicher. Eigenheit von mir.
		this.name = "";
		this.gehalt = 0;
	}
	
	public Angestellter(String name) {
		this.name = name;
		this.gehalt = 0;
	}
	
	public Angestellter(double gehalt) {
		this.name = "";
		this.gehalt = gehalt;
	}
	
	public Angestellter(String name, double gehalt) {
		this.name = name;
		this.gehalt = gehalt;
	}
	
	//Get & Set
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setGehalt(double gehalt) {
		this.gehalt = gehalt;
	}
	
	public double getGehalt() {
		return this.gehalt;
	}
	
}
