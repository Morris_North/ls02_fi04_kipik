/**
 * Created / Modified by 
 + @author Erkan Kipik
 * @date 2021-01-28
 * DO NOT COPY PASTE
 */
package angestelltenverwaltung;

public class AngestellterTest {
	
	public static void main(String[] args) {
		Angestellter ang1 = new Angestellter();
		Angestellter ang2 = new Angestellter();
		Angestellter ang3 = new Angestellter("Joschi", 2700);
		Angestellter ang4 = new Angestellter(4500);

		//Set Attributes
		ang1.setName("Meier");
		ang1.setGehalt(4500);
		ang2.setName("Petersen");
		ang2.setGehalt(6000);
		
		System.out.println("Name: "+ ang1.getName());
		System.out.println("Gehalt: "+ ang1.getGehalt() +" Euro");
		System.out.println("\nName: "+ ang2.getName());
		System.out.println("Gehalt: "+ ang2.getGehalt() +" Euro");
		System.out.println("\nName: "+ ang3.getName());
		System.out.println("Gehalt: "+ ang3.getGehalt() +" Euro");
		System.out.println("\nName: "+ ang4.getName());
		System.out.println("Gehalt: "+ ang4.getGehalt() +" Euro");
		
	}
	
}
