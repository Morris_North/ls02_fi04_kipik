package kreis;
import java.awt.Point;

public class Rechteck {
	private double a;
	private double b;
	private Point mittelpunkt;
	
	public Rechteck(double a, double b, Point mittlepunkt) {
		setA(a);
		setB(b);
		setMittelpunkt(mittlepunkt);
	}
	
	// SET
	public void setA(double a) {
		if(a < 0) {
			this.a = 0;
		}else {
			this.a = a;
		}
	}
	
	public void setB(double b)  {
		if(b < 0) {
			this.b = 0;
		}else {
			this.b = b;
		}
	}
	
	public void setMittelpunkt(Point mittelpunkt) {
		this.mittelpunkt = mittelpunkt; 
	}
	
	// GET
	public double getA() {
		return this.a;
	}
	
	public double getB() {
		return this.b;
	}
	
	public Point getMittelpunkt() {
		return this.mittelpunkt;
	}
	
	//Methoden
	public double getDiagonale() {
		return Math.sqrt(Math.pow(this.a, 2) + Math.pow(this.b, 2));
	}
	
	public double getFlaeche() {
		return this.a * this.b;
	}
	
	public double getUmfang() {
		return this.a*2 + this.b*2;
	}
}
