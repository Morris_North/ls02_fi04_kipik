package kreis;
import java.awt.Point;

public class RechteckTest {

	public static void main(String[] args) {
		Rechteck g = new Rechteck(5, 10, new Point(3,5));
		
		System.out.println("Kante A: "+g.getA()+" Kante B: "+g.getB());
		System.out.println("Diagonale: "+g.getDiagonale());
		System.out.println("Fl�che: "+g.getFlaeche());
		System.out.println("Umfang: "+g.getUmfang());
	}
}
